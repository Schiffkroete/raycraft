let objects = [];
const backgroundColor = 0;
let camera;
const time = Date.now()/1000;
const loopFrames = 360;
const fps = 60;

function setup() {
    createCanvas(240, 240);
    createLoop({
        duration:loopFrames/fps/4,
        framesPerSecond: fps,
        gif: true,
        gifRender: true,
        gifFileName: "animation.gif"
    });

    for(let x = -1; x <= 1; x++) {
        for(let y = -1; y <= 1; y++) {
            for(let z = -1; z <= 1; z++) {
                objects.push(new Sphere(new Vector3D(z*5, x*5, y*5), 2, 255));
            }
        }
    }
}
  
function draw() {
    let cPos1 = cos(Math.PI * 2 * frameCount/loopFrames);
    let cPos2 = 0;
    let cPos3 = frameCount % loopFrames < loopFrames * 0.5 ? sqrt(1-cPos1*cPos1) : -sqrt(1-cPos1*cPos1);
    let cPos = vector3D.mult(new Vector3D(cPos1, cPos2, cPos3), 17);

    let cDir1 = cos(Math.PI * 2 * frameCount/loopFrames);
    let cDir2 = 0;
    let cDir3 = frameCount % loopFrames < loopFrames * 0.5 ? sqrt(1-cDir1*cDir1) : -sqrt(1-cDir1*cDir1);
    let cDir = vector3D.mult(new Vector3D(cDir1, cDir2, cDir3), -1);
    camera = new Camera(cPos, cDir, 90);

    background(backgroundColor);
    let rays = camera.castRays();
    for(let y = 0; y < rays.length; y++) {
        for(let x = 0; x < rays[0].length; x++) {
            stroke(rays[y][x].march());
            point(x, y);
        }
    }

    print("frame: " + (frameCount-1) + " / " + loopFrames);
    
    if(frameCount === loopFrames*2+1) {
        print(Date.now()/1000 - time);
        noLoop();
    }
}