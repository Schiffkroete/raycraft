function Vector3D(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;

    this.distanceSquared = function(v) {
        let dx = this.x-v.x;
        let dy = this.y-v.y;
        let dz = this.z-v.z;
        return dx*dx + dy*dy + dz*dz;
    }

    this.distance = function(v) {
        return Math.sqrt(this.distanceSquared(v));
    }

    this.abs = function() {
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }

    this.normalize = function() {
        let length = this.abs();
        this.x = this.x / length;
        this.y = this.y / length;
        this.z = this.z / length;
    }

    this.mult = function(factor) {
        this.x = this.x * factor;
        this.y = this.y * factor;
        this.z = this.z * factor;
    }

    this.copy = function() {
        return new Vector3D(this.x, this.y, this.z);
    }
}

let vector3D = {};
vector3D.add = function(v1, v2) {
    return new Vector3D(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z);
}

vector3D.sub = function(v1, v2) {
    return new Vector3D(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z);
}

vector3D.mult = function(v, factor) {
    return new Vector3D(v.x * factor, v.y * factor, v.z * factor);
}

vector3D.cross = function(v1, v2) {
    let x = v1.y * v2.z - v1.z * v2.y;
    let y = v1.z * v2.x - v1.x * v2.z;
    let z = v1.x * v2.y - v1.y * v2.x;
    return new Vector3D(x, y, z);
}