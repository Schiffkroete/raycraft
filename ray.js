function Ray(pos, dir) {
    this.pos = pos;
    this.dir = dir;

    this.march = function(walkedDistance) {
        if(!walkedDistance) {
            walkedDistance = 0;
        }

        let nearestObject = null;
        let sd = Number.POSITIVE_INFINITY;
        for(let i = 0; i < objects.length; i++) {
            let newSd = objects[i].sdf(this.pos);
            if(newSd < sd) {
                sd = newSd;
                nearestObject = objects[i];
            }
        }

        if(sd > 1e3) {
            return backgroundColor;
        } else if(sd < 1e-5) {
            return nearestObject.color/(walkedDistance*walkedDistance / 80);
        } else {
            let step = this.dir.copy();
            step.mult(sd);
            this.pos = vector3D.add(this.pos, step);
            return this.march(walkedDistance + sd);
        }
    }
}