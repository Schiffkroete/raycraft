function Camera(pos, dir, fov) {
    this.pos = pos;
    this.fov = fov;
    this.dir = dir; // y muss 0 sein
    this.dir.normalize();

    let p = {};
    p.pos = vector3D.add(this.pos, this.dir);
    p.width = 2 * Math.tan((this.fov/360)*2*Math.PI*0.5);
    p.height = p.width * height / width;
    
    p.xdir = vector3D.cross(this.dir, new Vector3D(0, 1, 0));
    p.xdir.normalize();
    p.xdir.mult(p.width);
    p.xstep = vector3D.mult(p.xdir, 1/width);

    p.ydir = vector3D.cross(this.dir, p.xdir);
    p.ydir.normalize();
    p.ydir.mult(p.height);
    p.ystep = vector3D.mult(p.ydir, 1/height);

    this.plane = p;

    this.castRays = function() {
        let rays = [];
        for(let y = -height/2; y < height/2; y++) {
            let rayRow = [];
            for(let x = -width/2; x < width/2; x++) {
                let rayPos = this.pos.copy();
                let xOffset = this.plane.xstep.copy();
                xOffset.mult(x);
                let yOffset = this.plane.ystep.copy();
                yOffset.mult(y);
                let offset = vector3D.add(xOffset, yOffset);
                let intersection = vector3D.add(this.plane.pos, offset);

                let rayDir = vector3D.sub(intersection, this.pos);

                rayRow.push( new Ray(rayPos, rayDir) );
            }
            rays.push(rayRow);
        }
        return rays;
    }
}