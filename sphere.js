function Sphere(pos, radius, color) {
    this.pos = pos;
    this.radius = radius;
    this.color = color;

    this.sdf = function(rayPos) {
        return this.pos.distance(rayPos) - radius;
    }
}